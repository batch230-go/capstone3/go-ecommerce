
// Dependencies
const Products = require("../models/Products");
const auth = require("../auth.js");
const { response } = require("express");
const { all } = require("../routes/productRoutes");


// Creating or adding a product for admin only
module.exports.addProduct = (request, response) => {
	const hashedPassword = auth.decode(request.headers.authorization);
	let newProduct = new Products({
		name: request.body.name,
		description: request.body.description,
		type: request.body.type,
		price: request.body.price,
	});
	if(hashedPassword.isAdmin){
		return newProduct.save()
		.then(product => {
			console.log(product);
			response.send(product);
		})
		.catch(error => {
			console.log(error);
			response.send(false);
		});
	}
	else {
		return response.status(401).send("User must be ADMIN to access this functionality");
	}
}
// Get all products active and inactive. Admin rights only
module.exports.getAllProducts = (request, response) =>{
	const hashedPassword = auth.decode(request.headers.authorization);
	if(hashedPassword.isAdmin){
		return Products.find({})
		.then(result => response.send(result));
	}
	else{
		return response.send(false);
	}
}
// Archive or Unarchive a product
module.exports.archiveProduct = (request, response) =>{
	const hashedPassword = auth.decode(request.headers.authorization);
	let updateIsActiveField = {
		isActive: request.body.isActive
	}
	if(hashedPassword.isAdmin){
		return Products.findByIdAndUpdate(request.params.productId, updateIsActiveField)
		.then(result => {
			console.log(result);
			response.send(true);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		})
	}
	else{
		return response.send(false);
	}
}

// Update Product
module.exports.updateProduct = (request, response) =>{
	const hashedPassword = auth.decode(request.headers.authorization);
	if(hashedPassword.isAdmin){
		let updatedProductDetails = {
			name: request.body.name,
			description: request.body.description,
			type: request.body.type,
			price: request.body.price
		}
		return Products.findByIdAndUpdate(request.params.productId, updatedProductDetails, {new:true})
		.then(result =>{
			console.log(result);
			response.send(result);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		});
	}
	else{
		return response.send(false);
	}
}


// Get all products for all access rights
module.exports.getAllActiveProducts = (request, response) => {
	return Products.find({isActive: true})
	.then(result => {
		response.send(result);
		console.log(result);
	})
	.catch(error => {
		response.send(error);
		console.log(error);
	});
}
module.exports.getAllProducts = (request, response) => {
	return Products.find({})
	.then(result => {
		response.send(result);
		console.log(result);
	})
	.catch(error => {
		response.send(error);
		console.log(error);
	});
}
module.exports.getActivePizzaOnly = (request, response) => {
	return Products.find({$and: [{isActive: true}, {type: "pizza"}]})
	.then(result => {
		response.send(result);
		console.log(result);
	})
	.catch(error => {
		response.send(error);
		console.log(error);
	});
}
module.exports.getActivePastaOnly = (request, response) => {
	return Products.find({$and: [{isActive: true}, {type: "pasta"}]})
	.then(result => {
		response.send(result);
		console.log(result);
	})
	.catch(error => {
		response.send(error);
		console.log(error);
	});
}
module.exports.getActiveSidesOnly = (request, response) => {
	return Products.find({$and: [{isActive: true}, {type: "sides"}]})
	.then(result => {
		response.send(result);
		console.log(result);
	})
	.catch(error => {
		response.send(error);
		console.log(error);
	});
}
module.exports.getActiveBeveragesOnly = (request, response) => {
	return Products.find({$and: [{isActive: true}, {type: "beverages"}]})
	.then(result => {
		response.send(result);
		console.log(result);
	})
	.catch(error => {
		response.send(error);
		console.log(error);
	});
}
module.exports.searchMenu = (request, response) => {
	return Products.find({$and: [{isActive: true}, {name: {$regex: request.body.name, $options: "$i"}}]})
	.then(result => {
		response.send(result);
		console.log(result);
	})
	.catch(error => {
		response.send(error);
		console.log(error);
	});
}


// Retrieve Specific Menu
module.exports.getMenu = (request, response) =>{
	console.log(request.params.productId);

	return Products.findById(request.params.productId)
	.then(result => response.send(result));
}


// module.exports.clearOrderedProducts = async (request, response) => {
// 	const hassedPassword = await auth.decode(request.headers.authorization);
// 	if(hassedPassword.isAdmin){
// 		await Products.find({})
// 		.then(result => {
// 		const allOrders = result.map(product => product.orders).flat();
// 		allOrders.splice(0, allOrders.length);
// 		console.log(allOrders.length);
// 		response.send(allOrders);
// 		return result.save();
// 		})
// 		.catch(error => {
// 			console.log(error);
// 			response.send(error);
// 		})
// 	}
// 	else {
// 		return response.status(401).send("User must be ADMIN to access this functionality");
// 	}
// }

module.exports.deleteOrder = (req, res) => {
	const hassedPassword = auth.decode(req.headers.authorization);
 	if(hassedPassword.isAdmin){
		return Products.findById(req.params.orderId)
		.then(result => {
			result.orders.splice(0, result.orders.length);
			result.save();
			res.send(result);
		})
	}
}

module.exports.deleteMenu = (req, res) => {
    const hashedPassword = auth.decode(req.headers.authorization);
    if(hashedPassword.isAdmin){
        return Products.findByIdAndRemove(req.params.menuId)
        .then(result => {
			console.log(result)
			res.send(result)
		})
        .catch(error => res.send(error));
    }
	else{
		return res.send("No Admin Rights");
	}
}
