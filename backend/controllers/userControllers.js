
// Dependencies for password token
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Dependencies for database models
const Users = require("../models/Users.js");
const Products = require("../models/Products.js");
const Cart = require("../models/Cart.js");


// Register User
module.exports.registerUser = (request, response) => {
	Users.findOne({email: request.body.email})
    .then(result => {
        if(result != null && result.email == request.body.email){
            console.log("Account creation failed. Email already exist in the database");
            return response.status(400).send({error: "Email already exist"});
        }
        else{
            let newUser = new Users({
                fullName: request.body.fullName,
                email: request.body.email,
                password: bcrypt.hashSync(request.body.password, 10),
                mobileNo: request.body.mobileNo
            })
            return newUser.save()
            .then(user => {
                console.log(user);
                response.send(true);
            })
            .catch(error =>{
                console.log(error);
                response.send(false);
            })
	    }
	})
}


// User Login
module.exports.loginUser = (request, response) => {
    Users.findOne({email: request.body.email})
    .then(result => {
        if(result != null && result.email == request.body.email){
            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
            if(isPasswordCorrect){
                console.log("Login successful. Access token created");
                return response.send({accessToken: auth.createAccessToken(result)});
            }
            else{
                console.log("Login failed. Incorrect password");
                return response.send(false);
            }
        }
        else{
            console.log("Login failed. User not found");
            return response.send(false);
        }
    })
    .catch(error => response.send(error));
}


// Retrieve Users with token
module.exports.getUserDetails = (request, response) => {
	const decodedToken = auth.decode(request.headers.authorization);
	return Users.findById(decodedToken.id)
	.then(result => {
		result.password = "******";
		response.send(result);
	})
	.catch(error => response.send(error));
}


module.exports.checkEmailExists = (request, response) =>{
	return Users.find({email: request.body.email})
    .then(result =>{
		console.log(result);
		if(result.length > 0){
			return response.send(true);
		}
		else{
			return	response.send(false);
		}
	})
	.catch(error => response.send(error));
}


// Order Product
module.exports.orderMenu = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
    let productName = await Products.findById(request.body.productId)
    .then(result => result.name);
    let newData = {
        userId: userData.id,
        userEmail: userData.email,
        productId: request.body.productId,
        productName: productName,
        quantity: request.body.quantity
    }
    let productPrice = await Products.findById(request.body.productId)
    .then(result => result.price);
    let priceData = {
        productPrice: (productPrice)
    }
    let isUserUpdated = await Users.findById(newData.userId)
    .then(user => {
        user.orders.push({
            subTotal: (priceData.productPrice * request.body.quantity),
            productId: newData.productId,
            productName: newData.productName,
            quantity: newData.quantity,
        })
        user.totalAmount = (user.totalAmount + (priceData.productPrice * request.body.quantity));
        return user.save()
        .then(result => {
            console.log(result);
            return true;
        })
        .catch(error => {
            console.log(error);
            return false;
        })
    })
    console.log(isUserUpdated);
    let isProductUpdated = await Products.findById(newData.productId)
    .then(product => {
        product.orders.push({
            userId: newData.userId,
            userEmail: newData.userEmail,
            quantity: newData.quantity
        })
        return product.save()
        .then(result => {
            console.log(result);
            return true;
        })
        .catch(error => {
            console.log(error);
            return false;
        })
    })
    console.log(isProductUpdated);
    (isUserUpdated == true && isProductUpdated == true)? 
    response.send(true) : response.send(false);
}


// Check Orders
module.exports.getUserOrders = (request, response) => {
	const decodedToken = auth.decode(request.headers.authorization);
	return Users.findById(decodedToken.id)
	.then(result => {
		response.send(result.orders);
	})
	.catch(error => response.send(error));
}

// Get All Orders Admin Only
module.exports.getAllOrders = async (request, response) => {
	const hassedPassword = await auth.decode(request.headers.authorization);
	if(hassedPassword.isAdmin){
		await Products.find({})
		.then(result => {
		const allOrders = result.map(product => product.orders).flat();
		console.log(result);
		response.send(allOrders);
		})
		.catch(error => {
			console.log(error);
			response.send(error);
		})
	}
	else {
		return response.status(401).send("User must be ADMIN to access this functionality");
	}
}


// Update User to Admin (Admin Only)
// Change to Admin
module.exports.changeToAdminProfile = (request, response) =>{
	const hashedPassword = auth.decode(request.headers.authorization);
	let updateIsAdminField = {
		isAdmin: request.body.isAdmin
	}
	if(hashedPassword.isAdmin){
		return Users.findByIdAndUpdate(request.params.userId, updateIsAdminField)
		.then(result => {
			console.log(result);
			response.send(true);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		})
	}
	else{
		return response.send(false);
	}
}
// Get all users (Admin Only)
module.exports.getAllUsers = (request, response) => {
    const hashedPassword = auth.decode(request.headers.authorization);
    if(hashedPassword.isAdmin){
        return Users.find({email: { $ne: 'jordanjeffgo@mail.com' }})
        .then(result => {
            response.send(result);
            console.log(result);
        })
        .catch(error => {
            response.send(error);
            console.log(error);
        });
    }
    else{
        return response.send(false);
    }
}


// Complete User Orders (Admin Only)
module.exports.completeOrder = (req, res) => {
	const hassedPassword = auth.decode(req.headers.authorization);
 	if(hassedPassword.isAdmin){
		return Users.findById(req.params.userOrderId)
		.then(result => {
            result.orders.splice(0, result.orders.length);
            result.totalAmount = 0;
            result.save();
            res.send(result);
		})
	}
}

module.exports.deleteUser = (req, res) => {
    const hashedPassword = auth.decode(req.headers.authorization);
    if(hashedPassword.isAdmin){
        return Users.findByIdAndRemove(req.params.userId)
        .then(result => res.send(result))
        .catch(error => res.send(error));
    }
}
