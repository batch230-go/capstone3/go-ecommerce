
// Dependencies
const mongoose = require("mongoose");


// Model Schema
const cartSchema = new mongoose.Schema({
	userEmail: {
		type: String,
		required: [true, "User email is required"]
	},
    cartTotalAmount: {
		type: Number,
		default: 0
	},
	cartProducts: [{
		productId: {
			type: String,
			required: [true, "Product ID is required"]
		},
		productName: {
			type: String,
			required: [true, "Product name is required"]
		},
		quantity: {
			type: Number,
			default: 0
		},
		subTotal: {
			type: Number,
			default: 0
		}
	}]
});
module.exports = mongoose.model("Cart", cartSchema);