// Dependencies
const mongoose = require("mongoose");


// Model Schema
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	type: {
		type: String,
		required: [true, "Product type is required"]
	},
	price : {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		default: new Date()
	},
	orders : [
		{
            userId: {
				type: String,
				required: [true, "UserId is required"]
			},
            userEmail: {
				type: String,
				required: [true, "UserEmail is required"]
			},
			quantity: {
				type: Number,
				default: 0
			},
			orderStatus: {
				type: Boolean,
				default: true
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}		
    ]
});

module.exports = mongoose.model("Products", productSchema);
