
// Dependencies
const mongoose = require("mongoose");
const userControllers = require("../controllers/userControllers");
// const index = require("../index.js");

// Model Schema
const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, "Full name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String, 
		required: [true, "Mobile number is required"]
	},
    isAdmin: {
		type: Boolean,
		default: false
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	orders: [{
		productId: {
			type: String,
			required: [true, "Product ID is required"]
		},
		productName: {
			type: String,
			required: [true, "Product name is required"]
		},
		quantity : {
			type: Number,
			default: 0
		},
		subTotal: {
			type: Number,
			default: 0
		},
		orderStatus: {
			type: String,
			default: "Pending"
		},
		isOrderActive: {
			type: Boolean,
			default: true
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
	}]
});
module.exports = mongoose.model("Users", userSchema);

