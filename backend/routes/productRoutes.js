
// Dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productControllers = require("../controllers/productControllers.js");


// Add Product
router.post("/addProduct", auth.verify, productControllers.addProduct);
// Get All Products Active and Inactive
router.get("/all", auth.verify, productControllers.getAllProducts);
// Archive or Unarchive a Product
router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);
// Update Product
router.put("/update/:productId", auth.verify, productControllers.updateProduct);


// Get Active Products
router.get("/menus", productControllers.getAllActiveProducts);
// Get All Active and Inactive Products
router.get("/allMenu", productControllers.getAllProducts);
// Get Pizza Only
router.get("/pizza", productControllers.getActivePizzaOnly);
// Get Lasagna Only
router.get("/pasta", productControllers.getActivePastaOnly);
// Get Side Dishes Only
router.get("/sides", productControllers.getActiveSidesOnly);
// Get Beverages Only
router.get("/beverages", productControllers.getActiveBeveragesOnly);
// Search Menu
router.post("/search", productControllers.searchMenu);
// Get Specific Menu
router.get("/:productId", productControllers.getMenu);
// Delete All Orders
// router.delete("/admin/clearOrders", auth.verify, productControllers.clearOrderedProducts);
// Delete Orders ADMIN ONLY
router.delete("/admin/:orderId/deleteOrders", productControllers.deleteOrder);
// For Developer Only PRODUCT DB DELETION
router.delete("/admin/:menuId/deleteMenu", auth.verify, productControllers.deleteMenu);

module.exports = router;