
const auth = require("../auth.js");
const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");


// Routers for every task
// This is for registration
router.post("/register", userControllers.registerUser);

router.post("/login", userControllers.loginUser);

router.get("/details", auth.verify, userControllers.getUserDetails);

router.post("/checkEmail", userControllers.checkEmailExists);

router.post("/orderProduct", auth.verify, userControllers.orderMenu);

router.get("/myOrders", auth.verify, userControllers.getUserOrders);

router.get("/admin/allOrders", auth.verify, userControllers.getAllOrders);

router.patch("/admin/:userId", auth.verify, userControllers.changeToAdminProfile);

router.get("/admin/allUsers", auth.verify, userControllers.getAllUsers);

router.delete("/admin/:userOrderId/userOrders", auth.verify, userControllers.completeOrder);
// For Developer Only USER DB Deletion
router.delete("/admin/:userId/deleteUser", auth.verify, userControllers.deleteUser);

module.exports = router;