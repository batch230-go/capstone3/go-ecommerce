
import './App.css';
import styled, { keyframes } from "styled-components";

import { Container } from "react-bootstrap";
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router';
 
import AppNavbar from './components/AppNavbar';
import { useState, useEffect } from 'react';
import UserContext, { UserProvider } from './UserContext';

import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Profile from './pages/Profile';
import Settings from './pages/Settings';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import Pizza from './pages/Pizza';
import Pasta from './pages/Pasta';
import Sides from './pages/Sides';
import Beverages from './pages/Beverages';
import Search from './pages/Search';
import ProductView from './pages/ProductView';

import AdminDashboard from './pages/AdminDashboard';
import AdminOrderedProducts from './pages/AdminOrderedProducts';
import AdminAllUsers from './pages/AdminAllUsers';
import AdminInventory from './pages/AdminInventory';
import AdminDeleteData from './pages/AdminDeleteData';

export default function App(){

    const [user, setUser] = useState({
        email: localStorage.getItem('email'),
        id: localStorage.getItem('id'),
        isAdmin: localStorage.getItem('isAdmin'),
        totalAmount: localStorage.getItem(0)
    })
    // const unsetUser = () => {
    //     localStorage.clear();
    // }

    const unsetUser = () => {
        localStorage.clear();

        // setUser({
        //     id: null,
        //     email: null,
        //     isAdmin: null,
        //     totalAmount: null
        // })
    }

    // useEffect(() => {
    //     fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    //         headers: {
    //             Authorization: `Bearer ${localStorage.getItem('token')}`
    //         }
    //     })
    //     .then(res => res.json())
    //     .then(data => {
    //         if(typeof data_id !== undefined){
    //             localStorage.setItem('email', data.email);
    //             localStorage.setItem('id', data._id);
    //             localStorage.setItem('isAdmin', data.isAdmin);
    //             localStorage.setItem('totalAmount', data.totalAmount);
    //             setUser({
    //                 id: data._id,
    //                 isAdmin: JSON.stringify(data.isAdmin),
    //                 email: data.email,
    //                 totalAmount: data.totalAmount
    //             })
    //         }
    //         else{
    //             setUser({
    //                 id: null,
    //                 email: null,
    //                 isAdmin: null,
    //                 totalAmount: null
    //             })
    //         }
    //     })
    // }, []);

    return(
        <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
                <Container fluid>
                    <AppNavbar />
                    <div id="page-bg">
                        <Routes>
                            <Route path="/" element={<Home />} />
                            <Route path="/products" element={<Products />} />
                                <Route path="/search" element={<Search />} />
                                <Route path="/pizza" element={<Pizza />} />
                                <Route path="/pasta" element={<Pasta />} />
                                <Route path="/sides" element={<Sides />} />
                                <Route path="/beverages" element={<Beverages />} />
                                    <Route path="/products/:productId" element={<ProductView />} />
                            <Route path="/register" element={<Register />} />
                            <Route path="/login" element={<Login />} />
                            <Route path="/profile" element={<Profile />} />
                            <Route path="/admin" element={<AdminDashboard />} />
                                <Route path="/admin/orders" element={<AdminOrderedProducts />} />
                                <Route path="/admin/allUsers" element={<AdminAllUsers />} />
                                <Route path="/admin/inventory" element={<AdminInventory />} />
                                <Route path="/admin/resetDatabase" element={<AdminDeleteData />} />
                            <Route path="/settings" element={<Settings />} />
                            <Route path="/logout" element={<Logout />} />
                            <Route path="*" element={<NotFound />} />
                        </Routes>
                    </div>
                </Container>
            </Router>
        </UserProvider>
    )
}


