
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { Link, NavLink, Navigate } from 'react-router-dom';
import { Fragment, useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){

    const { user } = useContext(UserContext);

    return(
        <Navbar id="global-navbar" className="p-3 fixed-top" expand="lg">
            <Container fluid>
                <Navbar.Brand className="ps-3" as={NavLink} to="/">Emerald Pizza</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={NavLink} to="/" className='pe-5'>Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/products" className='pe-5'>Menu</Nav.Link>
                        {(user.email !== null)?
							<NavDropdown title={user.email} id="nav-dropdown-logout">
                            {(user.isAdmin === 'true')?
                                <Fragment>
                                    <NavDropdown.Item as={NavLink} to="/admin">Admin Dashboard</NavDropdown.Item>
                                    <NavDropdown.Item as={NavLink} to="/profile">Profile</NavDropdown.Item>
                                </Fragment>
                                :
                                <NavDropdown.Item as={NavLink} to="/profile">Profile</NavDropdown.Item>               
                            }
                            <NavDropdown.Item as={NavLink} to="/settings">Settings</NavDropdown.Item>
                            <NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
                            </NavDropdown>
							:
							<Fragment>
								<Nav.Link className="pe-5" as={NavLink} to="/login">Log In</Nav.Link>
								<Nav.Link className="pe-3" as={NavLink} to="/register">Sign Up</Nav.Link>
							</Fragment>
						}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}