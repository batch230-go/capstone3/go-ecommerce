
import { useContext, useState, useEffect } from "react";
import UserContext from '../UserContext';
import { Navigate, Link } from "react-router-dom";
import { Table, Container, Button } from "react-bootstrap";
import { Fragment } from "react";
import Swal from "sweetalert2";


export default function AdminAllUsers(){
    const { user } = useContext(UserContext);
    const [allUsers, setAllUsers] = useState([]);

    const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/admin/allUsers`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
		setAllUsers(data.map(user => {
				return (
					<tr key={user._id} className="d-flex">
						<td className="text-light col-3">{user.fullName}</td>
						<td className="text-light col-3">{user.email}</td>
						<td className="text-light col-2">{user.mobileNo}</td>
						<td className="text-light col-1">{user.isAdmin ? "Admin" : "User"}</td>
						<td className="text-light col-3 text-center">
                        <div className="">
                        {(user.isAdmin)?
                            (user.isAdmin && (user.email === 'admin@mail.com' || user.email === 'jordanjeffgo@mail.com'))?
                            <></>
                            :
                            <Button variant="danger" size="sm" onClick={() => toUser(user._id, user.fullName)}>Switch to User</Button>    
                        :  
                            <Button variant="success" size="sm" onClick={() => toAdmin(user._id, user.fullName)}>Switch to Admin</Button>
                        }   
                        </div>
						</td>
					</tr>
				)
			}));
		});
	}
    useEffect(()=>{
		fetchData();
	}, [])


    const toAdmin = (userId, userName) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/admin/${userId}`, {
            method: "PATCH",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isAdmin: true
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Successful",
                    icon: "success",
                    text: `${userName} is now an Admin!`
                });
                fetchData();
            }
            else{
                Swal.fire({
                    title: "Archive unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                });
            }
        })
    }
    const toUser = (userId, userName) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/admin/${userId}`, {
            method: "PATCH",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isAdmin: false
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Successful",
                    icon: "success",
                    text: `${userName} is now back to User Profile.`
                });
                fetchData();
            }
            else{
                Swal.fire({
                    title: "Archive unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                });
            }
        })
    }

    
    return(
        (user.isAdmin === 'true' && (user.email === 'admin@mail.com' || user.email === 'jordanjeffgo@mail.com'))
        ?
        <Container>
            <div className="pt-5 mb-3 text-center text-light">
                <h1>Admin Dashboard(All Users)</h1>
                <Button variant="success" className="mx-2" as={Link} to="/admin">Back to main dashboard</Button>
            </div>
            <Table striped bordered hover className="bg-dark">
                <thead>
                    <tr className="text-center d-flex">
                    <th className="col-3">Full Name</th>
                    <th className="col-3">User Email</th>
                    <th className="col-2">Mobile Number</th>
                    <th className="col-1">Status</th>
                    <th className="col-3">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {allUsers}
                </tbody>
            </Table>
        </Container>
        :
        <Navigate to="/products" />
    )
}