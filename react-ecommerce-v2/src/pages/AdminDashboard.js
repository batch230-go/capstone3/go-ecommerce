
import { useContext, useState, useEffect, Fragment } from "react";
import UserContext from '../UserContext';
import { Navigate, Link } from "react-router-dom";
import { Button, Table, Modal, Form, Container } from "react-bootstrap";
import Swal from "sweetalert2";


export default function AdminDashboard(){

	const { user } = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);
	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [type, setType] = useState(0);
    const [price, setPrice] = useState(0);
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	// To control the add course modal pop out
	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal


	const openEdit = (id) => {
		setProductId(id);
		fetch(`${ process.env.REACT_APP_API_URL }/products/${id}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
            setType(data.type);
			setPrice(data.price);
		});
		setShowEdit(true)
	};
	const closeEdit = () => {
	    setName('');
	    setDescription('');
        setType('');
	    setPrice(0);

		setShowEdit(false);
	};


	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(async data => {

		await setAllProducts(await data.map(product => {
				return (
					<tr key={product._id} className="d-flex">
						<td className="text-light col-2">{product.name}</td>
						<td className="text-light col-4">{product.description}</td>
						<td className="text-light col-2">{product.type}</td>
						<td className="text-light col-1">{product.price}</td>
						<td className="text-light col-1">{product.isActive ? "Active" : "Inactive"}</td>
						<td className="text-light col-2">
							{
								(product.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</>
							}
						</td>
					</tr>
				)
			}));
		});
	}
	useEffect(()=>{
		fetchData();
	}, [])


	const archive = (id, productName) =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}
	const unarchive = (id, productName) =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	const addProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();
		    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
                    type: type,
				    price: price
				})
		    })
		    .then(res => res.json())
		    .then(data => {

		    	if(data){
		    		Swal.fire({
		    		    title: "Menu succesfully Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});
		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the modal
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}
		    })
		    setName('');
		    setDescription('');
            setType('');
		    setPrice(0);
	}
	const editProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();
		    fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
                    type: type,
				    price: price
				})
		    })
		    .then(res => res.json())
		    .then(data => {

		    	if(data){
		    		Swal.fire({
		    		    title: "Menu succesfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now available`
		    		});
		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the form
		    		closeEdit();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeEdit();
		    	}
		    })
		    setName('');
		    setDescription('');
            setType('');
		    setPrice(0);
	} 

	// Submit button validation for add/edit course
	useEffect(() => {
        if(name !== "" && description !== "" && type !== "" && price > 0){
            setIsActive(true);
        } 
		else{
            setIsActive(false);
        }
    }, [name, description, type, price]);

	
	return(
		(user.isAdmin === 'true')
		?
		<Container>
			<div className="pt-5 mb-3 text-center text-light">
				<h1>Admin Dashboard</h1>
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Menu</Button>
				{(user.email === 'admin@mail.com' || user.email === 'jordanjeffgo@mail.com')?
					<Button variant="success" className="
					mx-2" as={Link} to="/admin/allUsers">All Users</Button>				
					:
					<></>
				}
				<Button variant="success" className="
				mx-2" as={Link} to="/admin/orders">Ordered Products</Button>
				<Button variant="success" className="
				mx-2" as={Link} to="/admin/inventory">Inventory</Button>
				{(user.email === 'jordanjeffgo@mail.com')?
					<Button variant="success" className="
					mx-2" as={Link} to="/admin/resetDatabase">Delete Data</Button>
					:
					<></>
				}
			</div>

			<Table striped bordered hover className="bg-dark text-light">
		      <thead>
		        <tr className="d-flex text-center">
		          <th className="col-2">Product Name</th>
		          <th className="col-4">Description</th>
		          <th className="col-2">Type</th>
		          <th className="col-1">Price</th>
		          <th className="col-1">Status</th>
		          <th className="col-2">Actions</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allProducts}
		      </tbody>
		    </Table>

	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd} >
				<Container className="d-flex-sm justify-content-center my-auto h-100">
					<Form onSubmit={e => addProduct(e)}>

						<Modal.Header closeButton>
							<Modal.Title>Add New Menu</Modal.Title>
						</Modal.Header>

						<Modal.Body>
							<Form.Group controlId="name" className="mb-3">
								<Form.Label>Product Name</Form.Label>
								<Form.Control 
									type="text" 
									placeholder="Enter Product Name" 
									value = {name}
									onChange={e => setName(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group controlId="description" className="mb-3">
								<Form.Label>Product Description</Form.Label>
								<Form.Control
									as="textarea"
									rows={3}
									placeholder="Enter Product Description" 
									value = {description}
									onChange={e => setDescription(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group controlId="type" className="mb-3">
								<Form.Label>Product Type</Form.Label>
								<Form.Select rows={3} value = {type} onChange={e => setType(e.target.value)} required>
									<option>Choose Product Type</option>
									<option>pizza</option>
									<option>pasta</option>
									<option>sides</option>
									<option>beverages</option>
								</Form.Select>
							</Form.Group>

							<Form.Group controlId="price" className="mb-3">
								<Form.Label>Price</Form.Label>
								<Form.Control 
									type="number" 
									placeholder="Enter Product Price" 
									value = {price}
									onChange={e => setPrice(e.target.value)}
									required
								/>
							</Form.Group>
						</Modal.Body>

						<Modal.Footer>
							{ isActive 
								? 
								<Button variant="primary" type="submit" id="submitBtn">
									Save
								</Button>
								: 
								<Button variant="danger" type="submit" id="submitBtn" disabled>
									Save
								</Button>
							}
							<Button variant="secondary" onClick={closeAdd}>
								Close
							</Button>
						</Modal.Footer>
					</Form>	
				</Container>
	    	</Modal>

			<Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e)}>
					<Container>				
						<Modal.Header closeButton>
							<Modal.Title>Edit a Menu</Modal.Title>
						</Modal.Header>

						<Modal.Body>
							<Form.Group controlId="name" className="mb-3">
								<Form.Label>Product Name</Form.Label>
								<Form.Control 
									type="text" 
									placeholder="Enter Product Name" 
									value = {name}
									onChange={e => setName(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group controlId="description" className="mb-3">
								<Form.Label>Product Description</Form.Label>
								<Form.Control
									as="textarea"
									rows={3}
									placeholder="Enter Product Description" 
									value = {description}
									onChange={e => setDescription(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group controlId="type" className="mb-3">
									<Form.Label>Product Type</Form.Label>
									<Form.Select value = {type} onChange={e => setType(e.target.value)} required>
										<option>Choose Product Type</option>
										<option>pizza</option>
										<option>pasta</option>
										<option>sides</option>
										<option>beverages</option>
									</Form.Select>
								</Form.Group>

							<Form.Group controlId="price" className="mb-3">
								<Form.Label>Course Price</Form.Label>
								<Form.Control 
									type="number" 
									placeholder="Enter Course Price" 
									value = {price}
									onChange={e => setPrice(e.target.value)}
									required
								/>
							</Form.Group>
						</Modal.Body>

						<Modal.Footer>
							{ isActive 
								? 
								<Button variant="primary" type="submit" id="submitBtn">
									Save
								</Button>
								: 
								<Button variant="danger" type="submit" id="submitBtn" disabled>
									Save
								</Button>
							}
							<Button variant="secondary" onClick={closeEdit}>
								Close
							</Button>
						</Modal.Footer>
					</Container>
				</Form>	
			</Modal>
		</Container>
		:
		<Navigate to="/products" />
    )
}