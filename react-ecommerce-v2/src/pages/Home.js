import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, Fragment } from "react";
import { Container, Card } from "react-bootstrap";
import banner from "../images/banner.png";


export default function Home(){

    const { user } = useContext(UserContext);

    return(
        <Fragment>
            <Container>
                <img src={banner} className="banner img-fluid" />
            </Container>

            <div className="home-page-size">

            </div>

            <div className="footer-color text-center">
                <div className="d-lg-inline-block d-md-inline-block col-lg-3 col-md-3  pt-5 pb-3">
                    <h3>About Us</h3>
                    <p>Mission & Vision</p>
                    <p>Founders</p>
                    <p>Branches</p>
                </div>
                <div className="d-lg-inline-block d-md-inline-block col-lg-3 col-md-3 pb-3">
                    <h3>Careers</h3>
                    <p>Pizza Chef</p>
                    <p>Manager</p>
                    <p>Accountant</p>
                </div>
                <div className="d-lg-inline-block d-md-inline-block col-lg-3 col-md-3 pb-3">
                    <h3>Follow Us</h3>
                    <p>Facebook</p>
                    <p>LinkedIn</p>
                    <p>Instagram</p>
                </div>
                <div className="d-lg-inline-block d-md-inline-block col-lg-3 col-md-3 pb-4">
                    <h3>Contact Us</h3>
                    <p>(+63)916-911-1111</p>
                    <p>(+63)918-922-2222</p>
                    <p>empizza@mail.com.ph</p>
                </div>
            </div>
        </Fragment>
    )
}