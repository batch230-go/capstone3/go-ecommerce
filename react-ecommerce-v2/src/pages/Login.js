import { EyeOutlined, EyeInvisibleOutlined } from '@ant-design/icons';
import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Login(){

    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [visible, setVisible] = useState(false);
    const [isActive, setIsActive] = useState(false);
    
    function loginUser(event){
        event.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Emeralds Pizza"
                })
            }
            else{
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                })
            }
        })
    }

    const retrieveUserDetails = (token) => {       
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {

            localStorage.setItem('email', data.email);
            localStorage.setItem('id', data._id);
            localStorage.setItem('isAdmin', data.isAdmin);
            localStorage.setItem('totalAmount', data.totalAmount);
            setUser({
                id: data._id,
                isAdmin: JSON.stringify(data.isAdmin),
                email: data.email,
                totalAmount: data.totalAmount
            })
        })
    }


    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password])

    return(
        (user.id !== null && user.email !== null)?
        <Navigate to ="/products" />
        :
        <div className="page-size d-flex align-items-center text-light">
            <Container id="page-opacity" className='login-form col-lg-3 col-sm-10 col-md-8'>
                <Form onSubmit={(event) => loginUser(event)}>
                <h2 className='pt-3 text-center'>Login</h2>
                    <Form.Group controlId="userEmail" className='pt-2'>
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value = {email}
                            onChange = {e => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password" className='pt-2'>
                        <Form.Label>Password</Form.Label>
                        <div className='d-flex'>
                            <Form.Control 
                                type={visible? "text" : "password"}
                                placeholder="Password" 
                                value = {password}
                                onChange = {e => setPassword(e.target.value)}
                                className = ''
                                required
                            />
                            <div className='p-2' onClick={() => setVisible(!visible)}>
                                {
                                    visible? <EyeOutlined /> : <EyeInvisibleOutlined />
                                }
                            </div>
                        </div>
                    </Form.Group>

                    <div className='pt-3 pb-3 text-center'>
                        { isActive ?
                            <Button variant="outline-light" type="submit" id="submit-btn-login-register">
                                Log In
                            </Button>
                            :
                            <Button variant="outline-light" type="submit" id="submit-btn-login-register" disabled>
                                Log In
                            </Button>
                        }  
                    </div>
                </Form>
            </Container> 
        </div>
    )
}
