
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import { useEffect, useContext } from 'react';

export default function Logout(){
    const { setUser, unsetUser } = useContext(UserContext);
    unsetUser();
    useEffect(() => {
        setUser({id: null, email: null, isAdmin: null, totalAmount: null});
    })

    return(
        <Navigate to="/login" />
    )
}


