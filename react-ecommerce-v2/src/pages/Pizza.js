
import ProductCard from '../components/ProductCard';
import { useEffect, useState, useContext } from "react";
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { Button, Container } from 'react-bootstrap';
import pizzaLogo from '../images/buttons/pizza.jpg';
import pastaLogo from '../images/buttons/pasta.jpg';
import sidesLogo from '../images/buttons/sides.jpg';
import beveragesLogo from '../images/buttons/beverages.jpg';


export default function Pizza(){

    const { user } = useContext(UserContext);
    const [pizza, setPizza] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/pizza`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setPizza(data.map(pizza => {
                return(
                    <ProductCard key={pizza.id} productProp={pizza} />
                )
            }))
        })
    }, []);

    return (
        <div>
            <Container>
                <h1 className="text-center pt-3">Pizza</h1>
                <div className='offset-lg-3 col-lg-6 mb-4'>
                    <Button id="logo-btn" className='col-3' as={Link} to='/pizza'>
                        <img src={pizzaLogo} className="logo-btn" />
                    </Button>
                    <Button id="logo-btn" className='col-3' as={Link} to='/pasta'>
                        <img src={pastaLogo} className="logo-btn" />
                    </Button>
                    <Button id="logo-btn" className='col-3' as={Link} to='/sides'>
                        <img src={sidesLogo} className="logo-btn" />
                    </Button>
                    <Button id="logo-btn" className='col-3' as={Link} to='/beverages'>
                        <img src={beveragesLogo} className="logo-btn" />
                    </Button>
                </div>
                <div className='row'>
                    {pizza}
                </div>
            </Container>
        </div>
    )
}