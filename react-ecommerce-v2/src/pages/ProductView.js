
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom"
import { useState, useEffect, useContext } from "react";
import unavailable from "../images/unavailable.png";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ProductView(){
	const { user } = useContext(UserContext);
	const { productId } = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(0);

	useEffect( () => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});
	}, [productId])

	// const handleIncrement = (productId) =>{
	// 	setQuantity(product => {
	// 		product.quantity++;
	// 	})
	// }
	// const handleDecrement = (productId) =>{

	// 	setQuantity(product => {
	// 		product.quantity--;
	// 	})
	// }


	const order = (productId) =>{
		fetch(`${ process.env.REACT_APP_API_URL }/users/orderProduct`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId,
                quantity: 1
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: "Successfully Ordered",
					icon: "success",
					text: "You have successfully ordered this menu."
				});
				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});
	}

    
	return(
			<Container className="pt-5">
				<Card id="card-view" className='text-white col-lg-4 col-md-6'>
				<Card.Img variant="top" src={unavailable} className="p-3" />
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>
						{/* <Card.Subtitle>Quantity</Card.Subtitle>
						<button type="button" onClick={()=> handleDecrement(productId)} className="input-group-text">-</button>
						<div className="form-control">{quantity}</div>
						<button type="button" onClick={()=> handleIncrement(productId)} className="input-group-text">+</button>
						<Card.Text></Card.Text> */}
						<Button variant="primary"  size="lg" onClick={() => order(productId)}>Order</Button>
					</Card.Body>		
				</Card>
			</Container>
	)
}